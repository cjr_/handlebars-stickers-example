const STICKERS_URL = 'http://localhost:3000/api/v1/stickers';

$(appReady);

function appReady() {
  const source = $("#entry-template").html();
  const template = Handlebars.compile(source);
  const context = {
    title: 'My New Post',
    body: 'This is my first post!'
  };
  const html = template(context);
  // $('main').append(html);

  getStickers().then(showStickers);
}

function getStickers() {
  return $.get(STICKERS_URL);
}

function showStickers(stickers) {
  const source = $("#stickers-template").html();
  const template = Handlebars.compile(source);
  const html = template({
    stickers
  });
  $('main').append(html);
}
