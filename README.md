# CRUD Client
---
* [x] Define and describe a template engine
* [x] Use handlebars on the client to render a template
* [x] Make an AJAX request to a CRUD API
* [x] Use handlebars on the client to render a template with data from an AJAX request
